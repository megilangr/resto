<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$data = [
				[
					'name' => 'admin',
					'guard_name' => 'web'
				],
				[
					'name' => 'owner',
					'guard_name' => 'web'
				],
				[
					'name' => 'kasir',
					'guard_name' => 'web'
				],
			];

			foreach ($data as $key => $value) {
				try {
					$roles = Role::firstOrCreate($value);
				} catch (\Exception $e) {
					//throw $th;
				}
			}
    }
}
