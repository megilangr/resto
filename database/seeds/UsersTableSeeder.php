<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$data = [
				[
					'name' => 'admin',
					'email' => 'admin@mail.com',
					'password' => bcrypt('admin'),
				],
				[
					'name' => 'owner',
					'email' => 'owner@mail.com',
					'password' => bcrypt('owner'),
				],
				[
					'name' => 'kasir',
					'email' => 'kasir@mail.com',
					'password' => bcrypt('kasir'),
				],
			];

			foreach ($data as $key => $value) {
				try {
					$user = User::firstOrCreate($value);
					$roles = Role::findOrFail($key + 1);
					$user->assignRole($roles->name);
				} catch (\Exception $e) {
					//
				}
			}
		}
		
}
