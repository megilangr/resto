<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Cafe Holiday</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('assets') }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/custom/login.css">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

</head>

<body class="text-center">
  <form action="{{ route('login') }}" class="form-signin" method="POST">
    @csrf
    <img class="mb-2" src="{{ asset('images/baseImage/logo.png') }}" alt="" width="100" height="100">
    <h1 class="h3 mb-3 font-weight-normal">Cafe Holiday</h1>
    <hr>
    <h4 class="h4 mb-3 font-weight-normal">Silahkan Login</h4>
    <label for="inputEmail" class="sr-only">E-Mail</label>
    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Masukan E-Mail" required autofocus>
  
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
  </form>

  <script src="{{ asset('assets') }}/jquery/jquery-3.5.1.min.js"></script>
  <script src="{{ asset('assets') }}/popper/popper.min.js"></script>
  <script src="{{ asset('assets') }}/js/bootstrap.min.js"></script>

</body>

</html>
