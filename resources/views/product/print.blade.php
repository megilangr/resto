
<style>
	.order {
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			border-collapse: collapse;
			width: 100%;
	}

	h4 {
		font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	}

	.order td, .order th {
			border: 1px solid #ddd;
			padding: 8px;
	}

	.header td, .header th {
			border: 0px !important;
			padding: 8px;
	}

	.order tr:nth-child(even){background-color: #f2f2f2;}

	.order tr:hover {background-color: #ddd;}

	.order th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #4CAF50;
			color: white;
	}
</style>
<h4 style="text-align: center">
	Print Seluruh Daftar Produk
</h4>
<hr>
<table class="order header" width="100%">
	<tr>
		<td width="15%">Tanggal Print</td>
		<td width="5%">:</td>
		<td>{{ date('d/m/Y') }}</td>
		<td></td>
		<td width="15%">Pengguna</td>
		<td width="5%">:</td>
		<td>{{ ucwords(auth()->user()->name) }}</td>
	</tr>
</table>
<br>
<table class="order" width="100%">
	<thead>
		<tr>
			<td>Nama</td>
			<td>Kategori</td>
			<td>Harga</td>
			<td>Deskripsi</td>
		</tr>
	</thead>
	<tbody>
		@foreach ($product as $item)
			<tr>
				<td>{{ $item->name }}</td>
				<td>{{ $item->category->name }}</td>
				<td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
				<td>{{ $item->description }}</td>
			</tr>
		@endforeach
	</tbody>
</table>




