@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">Form Edit Produk</h5>
      <div class="card-tools">
        <a href="{{ route('product.index') }}" class="btn btn-xs btn-danger">
          &ensp; <i class="fa fa-arrow-left"></i> &ensp;
          Kembali
        </a>
      </div>
    </div>
    <div class="card-body">
      <form action="{{ route('product.update', $edit->id) }}" method="post" enctype="multipart/form-data">
				@csrf
				@method('PUT')
        <div class="row">
          <div class="col-md-8 col-lg-8">
            <div class="form-group">
              <label for="" class="form-control-label">Nama Produk : </label>
              <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukan Nama Produk..." value="{{ $edit->name }}" required autofocus>
              <span class="invalid-feedback">
                {{ $errors->first('name') }}
              </span>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div class="form-group">
              <label for="" class="form-control-label">Kategori Produk : </label>
              <select name="category_id" id="category_id" class="form-control" required>
								<option value="">Pilih Kategori</option>
								@foreach ($category as $item)
									<option value="{{ $item->id }}" {{ $item->id == $edit->category_id ? 'selected':'' }}>{{ $item->name }}</option>
								@endforeach
              </select>
              <span class="invalid-feedback">
                {{ $errors->first('category_id') }}
              </span>
            </div>
          </div>
          <div class="col-md-12 col-lg-12">
            <div class="form-group">
              <label for="" class="form-control-label">Deskripsi Produk : </label>
              <textarea name="description" id="description" rows="2" class="form-control" placeholder="Masukan Deskripsi Produk" required>{{ $edit->description }}</textarea>
              <span class="invalid-feedback">
                {{ $errors->first('name') }}
              </span>
            </div>
          </div>
          <div class="col-md-8 col-lg-8">
            <div class="form-group">
              <label for="image">Foto Produk : </label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="image" name="image">
                  <label class="custom-file-label" id="text-image" for="image">Pilih Foto</label>
                </div>
							</div>
							Foto Saat Ini &ensp; : &ensp;
							<a href="{{ asset('upload') }}/product/{{ $edit->image }}" target="_blank">
								{{ $edit->image }}
							</a>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div class="form-group">
              <label for="">Harga Produk : </label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="">Rp. </span>
                </div>
                <input type="number" name="price" id="price" class="form-control" value="{{ $edit->price }}" required min="0" placeholder="0">
              </div>
            </div>
          </div>
          <div class="col-md-2 col-lg-2">
            <div class="form-group">
              <button type="submit" class="btn btn-success btn-sm btn-block">
                <i class="fa fa-check"></i> &ensp;
                Tambah Data
              </button>
            </div>
          </div>
          <div class="col-md-2 col-lg-2">
            <div class="form-group">
              <button type="submit" class="btn btn-danger btn-sm btn-block">
                <i class="fa fa-undo"></i> &ensp;
                Reset Input
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(function() {
		$('#image').on('change', function() {
			$('#text-image').text(this.files && this.files.length ? this.files[0].name : 'Pilih Foto');
		});
	});
</script>
@endsection