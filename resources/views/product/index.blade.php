@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">
        Data Produk
      </h5>
      <div class="card-tools">
				<a href="{{ route('product.print') }}" target="_blank" class="btn btn-xs btn-success">
					<i class="fa fa-print"></i> &ensp; Print Data
				</a>
				<a href="{{ route('product.create') }}" class="btn btn-xs btn-primary"> &ensp; <i class="fa fa-plus"></i> &ensp; Tambah Data Produk</a>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="category-table">
          <thead>
            <tr>
              <th>Nama Produk</th>
              <th>Kategori</th>
              <th width="40%">Deskripsi</th>
              <th>Harga</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
						@forelse ($product as $item)
							<tr>
								<td>{{ $item->name }}</td>
								<td>{{ $item->category->name }}</td>
								<td>{{ $item->description }}</td>
								<td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
								<td width="10%" class="text-center">
									<div class="btn-group">
										<a href="{{ route('product.edit', $item->id) }}" class="btn btn-sm btn-warning">
											<i class="fa fa-edit"></i>
										</a>
										<form action="{{ route('product.destroy', $item->id) }}" method="post">
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-sm btn-danger">
												<i class="fa fa-trash"></i>
											</button>
										</form>
									</div>
								</td>
							</tr> 
						@empty
							<tr>
								<td class="text-center" colspan="5">
									<h5>Belum Ada Data</h5>
								</td>
							</tr>
						@endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    $('#category-table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection