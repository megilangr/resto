@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">
        Data Pengguna
      </h5>
      <div class="card-tools">
				{{-- <a href="{{ route('user.print') }}" target="_blank" class="btn btn-xs btn-success">
					<i class="fa fa-print"></i> &ensp; Print Data
				</a> --}}
				<a href="{{ route('user.create') }}" class="btn btn-xs btn-primary"> &ensp; <i class="fa fa-plus"></i> &ensp; Tambah Data Produk</a>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="category-table">
          <thead>
            <tr>
							<th>No.</th>
              <th>Nama Pengguna</th>
              <th>E-Mail</th>
              <th>Hak Akses</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
						@forelse ($user as $item)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->email }}</td>
								<td>{{ $item->roles->first()->name }}</td>
								<td width="10%" class="text-center">
									<div class="btn-group">
										@if ($item->id == auth()->user()->id)
											<a href="#" class="btn btn-sm btn-grey disabled" disabled>
												<i class="fa fa-times"></i>
											</a>
										@else
											<a href="{{ route('user.edit', $item->id) }}" class="btn btn-sm btn-warning">
												<i class="fa fa-edit"></i>
											</a>
											<form action="{{ route('user.destroy', $item->id) }}" method="post">
												@csrf
												@method('DELETE')
												<button type="submit" class="btn btn-sm btn-danger">
													<i class="fa fa-trash"></i>
												</button>
											</form>
										@endif
									</div>
								</td>
							</tr> 
						@empty
							<tr>
								<td class="text-center" colspan="5">
									<h5>Belum Ada Data</h5>
								</td>
							</tr>
						@endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    $('#category-table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection