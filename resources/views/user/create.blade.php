@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">Form Tambah Data Pengguna</h5>
      <div class="card-tools">
        <a href="{{ route('user.index') }}" class="btn btn-xs btn-danger">
          &ensp; <i class="fa fa-arrow-left"></i> &ensp;
          Kembali
        </a>
      </div>
    </div>
    <div class="card-body">
      <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <div class="form-group">
              <label for="" class="form-control-label">Nama Pengguna : </label>
              <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukan Nama Pengguna..." value="{{ old('name') }}" required autofocus>
              <span class="invalid-feedback">
                {{ $errors->first('name') }}
              </span>
            </div>
					</div>
					<div class="col-md-6 col-lg-6">
            <div class="form-group">
              <label for="" class="form-control-label">E-Mail : </label>
              <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" placeholder="Masukan E-Mail Pengguna..." value="{{ old('email') }}" required>
              <span class="invalid-feedback">
                {{ $errors->first('email') }}
              </span>
            </div>
          </div>
					<div class="col-md-4 col-lg-4">
            <div class="form-group">
              <label for="" class="form-control-label">Password : </label>
              <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Masukan Nama Pengguna..." value="{{ old('password') }}" required>
              <span class="invalid-feedback">
                {{ $errors->first('password') }}
              </span>
            </div>
          </div>
					<div class="col-md-4 col-lg-4">
            <div class="form-group">
              <label for="" class="form-control-label">Konfirmasi Ulang Password : </label>
              <input type="password" name="password_confirmation" id="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid':'' }}" placeholder="Masukan Nama Pengguna..." value="{{ old('password_confirmation') }}" required>
              <span class="invalid-feedback">
                {{ $errors->first('password_confirmation') }}
              </span>
            </div>
          </div>
          <div class="col-md-4 col-lg-4">
            <div class="form-group">
              <label for="" class="form-control-label">Hak Akses Pengguna : </label>
              <select name="role" id="role" class="form-control" required>
								<option value="">Pilih Hak Akses</option>
								@foreach ($role as $item)
									<option value="{{ $item->name }}" {{ $item->name == old('role') ? 'selected':'' }}>{{ $item->name }}</option>
								@endforeach
              </select>
              <span class="invalid-feedback">
                {{ $errors->first('role') }}
              </span>
            </div>
          </div>
          <div class="col-md-2 col-lg-2">
            <div class="form-group">
              <button type="submit" class="btn btn-success btn-sm btn-block">
                <i class="fa fa-check"></i> &ensp;
                Tambah Data
              </button>
            </div>
          </div>
          <div class="col-md-2 col-lg-2">
            <div class="form-group">
              <button type="submit" class="btn btn-danger btn-sm btn-block">
                <i class="fa fa-undo"></i> &ensp;
                Reset Input
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection