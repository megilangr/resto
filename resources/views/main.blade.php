@extends('layouts.master')

@section('content')
<div class="col-xs-12 col-md-6 col-lg-3">
  <div class="info-box">
    <span class="info-box-icon bg-info elevation-1"><i class="fa fa-list-alt"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Data Menu</span>
      <span class="info-box-number">
        {{ $product }}
      </span>
    </div>
    <!-- /.info-box-content -->
  </div>
</div>
<div class="col-xs-12 col-md-6 col-lg-3">
  <div class="info-box">
    <span class="info-box-icon bg-success elevation-1"><i class="fa fa-shopping-cart"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Total Transaksi</span>
      <span class="info-box-number">
        {{ $count }}
      </span>
    </div>
    <!-- /.info-box-content -->
  </div>
</div>
<div class="col-xs-12 col-md-12 col-lg-6">
  <div class="info-box">
    <span class="info-box-icon bg-primary elevation-1"><i class="fa fa-coins"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">Pendapatan Harian</span>
      <span class="info-box-number">
        Rp. {{ number_format($total, 0, ',', '.') }}
      </span>
    </div>
    <!-- /.info-box-content -->
  </div>
</div>
@endsection