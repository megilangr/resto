<nav class="mt-3">
    <ul class="nav nav-pills nav-sidebar flex-column nav-legacy" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
                <i class="nav-icon fa fa-home"></i>
                <p>Dashboard</p>
            </a>
				</li>
				@if (auth()->user()->roles->first()->name == 'admin')
        <li class="nav-header" style="padding-top: 15px;">Master Data</li>
        <li class="nav-item">
            <a href="{{ route('category.index') }}" class="nav-link">
                <i class="nav-icon fa fa-edit"></i>
                <p>
                    Kategori
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('product.index') }}" class="nav-link">
                <i class="nav-icon fa fa-edit"></i>
                <p>
                    Produk
                </p>
            </a>
				</li>
				@endif
				
				@if (auth()->user()->roles->first()->name == 'kasir' || auth()->user()->roles->first()->name == 'admin')
        <li class="nav-header" style="padding-top: 10px;">Transaksi</li>
        <li class="nav-item">
            <a href="{{ route('pos.index') }}" class="nav-link">
                <i class="nav-icon fa fa-shopping-cart"></i>
                <p> Kasir</p>
            </a>
				</li>
				@endif
				
				<li class="nav-header" style="padding-top: 10px;">Laporan</li>
				@if (auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'kasir' || auth()->user()->roles->first()->name == 'owner')
        <li class="nav-item">
            <a href="{{ route('pos.list') }}" class="nav-link">
                <i class="nav-icon fa fa-cubes"></i>
                <p>Data Penjualan</p>
            </a>
        </li>
				@endif
				@if (auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'owner')
        <li class="nav-item">
            <a href="{{ route('product.print') }}" target="_blank" class="nav-link">
                <i class="nav-icon fa fa-cubes"></i>
                <p>Data Produk</p>
            </a>
				</li>
				@endif
				@if (auth()->user()->roles->first()->name == 'admin')				
        <li class="nav-header" style="padding-top: 10px;">Lainnya</li>
        <li class="nav-item">
            <a href="{{ route('user.index') }}" class="nav-link">
                <i class="nav-icon fa fa-users text-pink"></i>
                <p>Master Pengguna</p>
            </a>
				</li>
				@endif
    </ul>
</nav>
