<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cafe Holiday</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('assets') }}/dist/css/adminlte.min.css">
    <link href="{{ asset('assets') }}/css/SourceSansPro.css" rel="stylesheet">

		<!-- Toastr -->
		<link rel="stylesheet" href="{{ asset('assets') }}/plugins/toastr/toastr.min.css">
	
		@yield('css')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-dark navbar-navy">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fa fa-user"></i> &ensp;
                        {{ auth()->user()->name }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">
                            <h5>Cafe Holiday</h5>
                            <img src="{{ asset('images') }}/baseImage/logo.png" alt="RestoLogo"
                                class="brand-image img-fluid img-circle elevation-3" style="height: 100px !important;">
                        </span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item text-center">
                            Halo, {{ auth()->user()->name }}
                        </a>
												<div class="dropdown-divider"></div>
												<a href="#" class="dropdown-item dropdown-footer" data-toggle="modal" data-target="#change-password">Ganti Password</a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}"
													onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
													class="dropdown-item dropdown-footer">Logout</a>
												<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
													@csrf
												</form>
                    </div>
                </li>
            </ul>
        </nav>

        <aside class="main-sidebar elevation-4 sidebar-light-navy">
            <a href="{{ route('home') }}" class="brand-link">
                <img src="{{ asset('images') }}/baseImage/logo.png" alt="RestoLogo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">&ensp; Cafe Holiday</span>
            </a>

            <div class="sidebar">

                @include('layouts.sidebar')
            </div>
        </aside>

        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>
                                {{ isset($title) ? $title : 'Cafe Holiday' }} <small style="font-size: 13px;"> | ver 1.0-1 </small>
                            </h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="home">Beranda</a></li>
                                <li class="breadcrumb-item active">Cafe Holiday</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </section>

            <section class="content">
                <div class="row">
                    @yield('content')
                </div>
            </section>
        </div>
        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2020 <a href="#">Cafe Holiday</a>.</strong> All rights
            reserved.
        </footer>

        <aside class="control-sidebar control-sidebar-dark">
        </aside>
		</div>

		<form action="{{ route('change-password') }}" method="post">
			@csrf
			<div class="modal fade" id="change-password" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ganti Password</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<label for="">Password Lama : </label>
										<input type="password" name="old_password" id="old_password" class="form-control" placeholder="Masukan Password Lama..." required>
									</div>
									<div class="form-group">
										<label for="">Password Baru : </label>
										<input type="password" name="password" id="password" class="form-control" placeholder="Masukan Password Baru..." required>
									</div>
									<div class="form-group">
										<label for="">Tulis Ulang Password Baru : </label>
										<input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Masukan Password Lama..." required>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup / Reset</button>
							<button type="submit" class="btn btn-success">Ubah Password</button>
						</div>
					</div>
				</div>
			</div>
		</form>


    <script src="{{ asset('assets') }}/plugins/jquery/jquery.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{ asset('assets') }}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
		<script src="{{ asset('assets') }}/dist/js/adminlte.min.js"></script>
		

		<!-- Toastr -->
		<script src="{{ asset('assets') }}/plugins/toastr/toastr.min.js"></script>

		@if (session()->has('success'))
		<script>
			toastr.success("{{ session('success') }}", 'Berhasil !');
		</script>
		@endif

		@if (session()->has('warning'))
		<script>
			toastr.warning("{{ session('warning') }}", 'Peringatan !');
		</script>
		@endif

		@if (session()->has('info'))
		<script>
			toastr.info("{{ session('info') }}", 'Informasi !');
		</script>
		@endif

		@if (session()->has('error'))
		<script>
			toastr.error("{{ session('error') }}", 'Kesalahan !');
		</script>
		@endif

		@yield('script')
</body>

</html>
