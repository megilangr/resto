@extends('layouts.master-pos')

@section('css')
<style>
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }
</style>
@endsection

@section('content')
<div class="col-md-6 col-lg-6 mt-4">
	@livewire('pos-product-list')
</div>
<div class="col-md-6 col-lg-6 mt-4">
	<div class="card card-outline card-secondary">
    <div class="card-header">
      <h5 class="card-title"> <i class="fa fa-shopping-cart text-purple"></i> &ensp; Transaksi</h5>
    </div>
    <div class="card-body p-0">
		@livewire('cart-item')
		<div class="row m-0 ">
			<div class="col-6 p-0">
				<a href="{{ route('pos.store') }}" onclick="event.preventDefault(); document.getElementById('selesai').submit(); setTimeout(() => { location.reload()}, 2000);" class="btn btn-success btn-block" style="border-radius: 0px;">
					<i class="fa fa-check"></i>
					Selesai
				</a>
			</div>
			<div class="col-6 p-0">
				<a href="{{ route('pos.reset') }}" class="btn btn-danger btn-block" style="border-radius: 0px;">
					<i class="fa fa-times"></i>
					Batalkan
				</a>
			</div>
			<form action="{{ route('pos.store') }}" method="post" id="selesai" target="_blank">
				@csrf
			</form>
		</div>
	
		</div>
	</div>
</div>
@endsection