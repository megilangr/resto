
<style>
	.order {
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			border-collapse: collapse;
			width: 100%;
	}

	.order td, .order th {
			border: 1px solid #ddd;
			padding: 8px;
	}

	.header td, .header th {
			border: 0px !important;
			padding: 8px;
	}

	.order tr:nth-child(even){background-color: #f2f2f2;}

	.order tr:hover {background-color: #ddd;}

	.order th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #4CAF50;
			color: white;
	}
</style>
<table class="order header" width="100%">
	<tr>
		<td width="15%">Tanggal</td>
		<td width="5%">:</td>
		<td>{{ date('d/m/Y', strtotime($pos->date)) }}</td>
		<td></td>
		<td width="15%">Pengguna</td>
		<td width="5%">:</td>
		<td>{{ ucwords($pos->user->name) }}</td>
	</tr>
</table>
<br>
<table class="order" width="100%">
	<thead>
		<tr>
			<td>Menu</td>
			<td>Jumlah</td>
			<td>Harga</td>
			<td>Subtotal</td>
		</tr>
	</thead>
	<tbody>
		@foreach ($pos->detail as $item)
			<tr>
				<td>{{ $item->product->name }}</td>
				<td>{{ $item->qty }}</td>
				<td>Rp. {{ number_format($item->price, 0, ',', '.') }}</td>
				<td>Rp. {{ number_format($item->qty * $item->price, 0, ',', '.') }}</td>
			</tr>
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" style="text-align: right;">Total : </td>
			<td>Rp. {{ number_format($pos->total, 0, ',', '.') }}</td>
		</tr>
	</tfoot>
</table>




