
<style>
	.order {
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			border-collapse: collapse;
			width: 100%;
	}

	h4 {
		font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	}

	.order td, .order th {
			border: 1px solid #ddd;
			padding: 8px;
	}

	.header td, .header th {
			border: 0px !important;
			padding: 8px;
	}

	.order tr:nth-child(even){background-color: #f2f2f2;}

	.order tr:hover {background-color: #ddd;}

	.order th {
			padding-top: 12px;
			padding-bottom: 12px;
			text-align: left;
			background-color: #4CAF50;
			color: white;
	}
</style>
<h4 style="text-align: center">
	Print Data Penjualan
</h4>
<hr>
<table class="order header" width="100%">
	<tr>
		<td width="30%">Tanggal Print</td>
		<td width="5%">:</td>
		<td>{{ date('d/m/Y') }}</td>
		<td></td>
		<td width="15%">Pengguna</td>
		<td width="5%">:</td>
		<td>{{ ucwords(auth()->user()->name) }}</td>
	</tr>
	@if (request()->get('date'))
		<tr>
			<td width="30%">Penjualan Tanggal</td>
			<td width="5%">:</td>
			<td colspan="5">{{ date('d/m/Y', strtotime(request()->get('date'))) }}</td>
		</tr>
	@endif
	@if (request()->get('month'))
		<tr>
			<td width="30%">Penjualan Tanggal</td>
			<td width="5%">:</td>
			<td colspan="5">{{ date('M Y', strtotime(request()->get('month'))) }}</td>
		</tr>
	@endif

</table>
<br>
<table class="order" width="100%">
	<thead>
		<tr>
			<th>No.</th>
			<th>Tanggal</th>
			<th>Jumlah</th>
			<th>Penginput</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		@forelse ($pos as $item)
			<tr>
				<td>{{ $loop->iteration }}.</td>
				<td>{{ date('d/m/Y', strtotime($item->date)) }}</td>
				<td>{{ count($item->detail) }} Produk</td>
				<td>{{ $item->user->name }}</td>
				<td>Rp. {{ number_format($item->total, 0, ',', '.') }}</td>
			</tr> 
		@empty
			<tr>
				<td class="text-center" colspan="5">
					<h5>Belum Ada Data</h5>
				</td>
			</tr>
		@endforelse
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4" style="text-align: right; background-color: #b1b1b1;">Total : </td>
			<td style="background-color: #b1b1b1;">Rp. {{ number_format($pos->sum('total'), 0, ',', '.') }}</td>
		</tr>
	</tfoot>
</table>




