@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">
        Data Penjualan
      </h5>
      <div class="card-tools">
				<a href="{{ route('pos.index') }}" class="btn btn-xs btn-primary"> &ensp; <i class="fa fa-shopping-cart"></i> &ensp; Transaksi POS</a>
      </div>
    </div>
    <div class="card-body">
			@if (auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'owner')
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Print Harian :</label>
						<form action="{{ route('pos.printAll') }}" method="GET">
							<div class="input-group mb-3">
								<input type="date" class="form-control" name="date" required>
								<div class="input-group-append">
									<button type="submit" class="btn bg-teal"><i class="fa fa-print"></i> &ensp; Print</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Print Bulanan :</label>
						<form action="{{ route('pos.printAll') }}" method="GET">
							<div class="input-group mb-3">
								<input type="month" class="form-control" name="month" required>
								<div class="input-group-append">
									<button type="submit" class="btn bg-teal"><i class="fa fa-print"></i> &ensp; Print</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			@endif
			<hr>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="category-table">
          <thead>
            <tr>
              <th>No.</th>
              <th>Tanggal</th>
							<th>Jumlah</th>
							<th>Total</th>
							<th>Penginput</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
						@forelse ($pos as $item)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ date('d/m/Y', strtotime($item->date)) }}</td>
								<td>{{ count($item->detail) }} Produk</td>
								<td>Rp. {{ number_format($item->total, 0, ',', '.') }}</td>
								<td>{{ $item->user->name }}</td>
								<td width="12%" class="text-center">
									<div class="btn-group">
										<a href="{{ route('pos.print', $item->id) }}" target="_blank" class="btn btn-xs btn-success">
											<i class="fa fa-print"></i> &ensp; Print Data
										</a>
									</div>
								</td>
							</tr> 
						@empty
							<tr>
								<td class="text-center" colspan="5">
									<h5>Belum Ada Data</h5>
								</td>
							</tr>
						@endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    $('#category-table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
		});
    
  });
</script>
@endsection