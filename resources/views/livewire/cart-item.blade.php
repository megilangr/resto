<div>
	<div class="cart-table" style="max-height: 400px !important; overflow-y: auto; overflow-x: hidden;">
		<div class="table-responsive">
			<table class="table mb-1">
				<thead>
					<tr>
						<th class="text-center">Nama Produk</th>
						<th class="text-center">Jumlah</th>
						<th class="text-center">Harga</th>
						<th class="text-center">SubTotal</th>
						<th class="text-center"></th>
					</tr>
				</thead>
				<tbody>
					@forelse ($cart as $key => $value)
					<tr wire:key="idcart-{{ $key }}">
						<td class="text-center">{{ $value['name'] }}</td>
						<td class="text-center">
							<div class="form-group">
								<div class="btn-group">
									<a href="#" class="btn btn-outline-danger btn-sm" wire:click="kurang({{ $value['id'] }})">
										<i class="fa fa-minus"></i>
									</a>
									<input type="number" name="qty" wire:model="cart.{{ $key }}.quantity" class="form-control form-control-sm bg-white" style="width: 40px !important; border-radius: 0px !important;" readonly >
									<a href="#" class="btn btn-outline-success btn-sm" wire:click="tambah({{ $value['id'] }})">
										<i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
						</td>
						<td class="text-center">Rp. {{ number_format($value['price'], 0, ',', '.') }}</td>
						<td class="text-center">Rp. {{ number_format($value['subtotal'], 0, ',', '.') }}</td>
						<td class="text-center">
							<a href="#" class="btn btn-xs btn-danger" wire:click="deleteitem({{ $value['id'] }})">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
					@empty
					<tr>
						<td colspan="5" class="text-center">
							Transaksi Kosong
						</td>
					</tr>
					@endforelse
				</tbody>
			</table>
		</div>
		<hr class="mt-0 mb-1">
	</div>
	<form class="form-horizontal">
		<div class="form-group row">
			<label class="col-7 col-form-label text-right">
				Total : 
			</label>
			<label class="col-5 col-form-label text-center pr-4">
				<u>
					Rp. {{ number_format($total, 0, ',', '.') }}
				</u>
			</label>
		</div>
		<div class="form-group row">
			<label class="col-7 col-form-label text-right">
				Jumlah Bayar : 
			</label>
			<div class="col-5 pr-4">
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text" id="">Rp. </span>
					</div>
					<input type="number" wire:model="bayar" name="price" id="price" class="form-control" required min="0" placeholder="0">
				</div>
			</div>
		</div>
		<div class="form-group row">
			<label class="col-7 col-form-label text-right">
				Kembalian : 
			</label>
			<label class="col-5 col-form-label text-center pr-4">
				Rp. {{ number_format($kembalian, 0, ',', '.') }}
			</label>
		</div>
	</form>
</div>
