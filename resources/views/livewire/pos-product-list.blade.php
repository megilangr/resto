<div>
  <div class="card card-outline card-success">
    <div class="card-header">
			<h5 class="card-title"> <i class="fa fa-list text-success"></i> &ensp; Daftar Menu</h5>
    </div>
    <div class="card-body pt-2">
      <div class="row">
        <div class="col-12 mb-2">
          <input type="text" wire:model.debounce.700ms="cari" id="cari" class="form-control" placeholder="Cari Nama Produk..">
				</div>
				@forelse ($product as $item)
        <div class="col-4 pt-2 pb-2 text-center">
					<div class="card" style="width: 100%; border-top: 1px solid #b1b1b1;">
						<img src="{{ $item->image == null ? asset('images/baseImage/no-img.png') : asset('upload/product').'/'.$item->image }}" alt="" class="img-fluid card-img-top pt-2" style="margin: 0 auto !important; max-height: 100%; max-width: 100%;" wire:click="select({{ $item->id }})">
						<ul class="list-group list-group-flush mt-2" style="border-top: 1px solid #b1b1b1;">
							<li class="list-group-item pt-0 pb-0">
								<h6 class="pt-1 pb-1 mb-0" style="border-bottom: 1px solid #b1b1b1;">{{ $item->name }}</h6>
								<h6 class="pt-1 pb-1">Rp. {{ number_format($item->price, 0, ',', '.') }}</h6>
							</li>
						</ul>
					</div>
        </div>
				@empty
				<div class="col-12 text-center">
					<h5>Tidak Ada Data Produk</h5>
				</div>
				@endforelse
      </div>
    </div>
  </div>
</div>
