@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">Form Edit Kategori</h5>
      <div class="card-tools">
        <a href="{{ route('category.index') }}" class="btn btn-xs btn-danger">
          &ensp; <i class="fa fa-arrow-left"></i> &ensp;
          Kembali
        </a>
      </div>
    </div>
    <div class="card-body">
      <form action="{{ route('category.update', $edit->id) }}" method="post">
				@csrf
				@method('PUT')
        <div class="row">
          <div class="col-md-8 col-lg-8">
            <div class="form-group">
              <label for="" class="form-control-label">Nama Kategori : </label>
              <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Masukan Nama Kategori..." value="{{ $edit->name }}" required autofocus>
              <span class="invalid-feedback">
                {{ $errors->first('name') }}
              </span>
            </div>
          </div>
          <div class="col-md-4 col-lg-4"></div>
          <div class="col-md-2 col-lg-2">
            <div class="form-group">
              <button type="submit" class="btn btn-success btn-sm btn-block">
                <i class="fa fa-check"></i> &ensp;
                Tambah Data
              </button>
            </div>
          </div>
          <div class="col-md-2 col-lg-2">
            <div class="form-group">
              <button type="submit" class="btn btn-danger btn-sm btn-block">
                <i class="fa fa-undo"></i> &ensp;
                Reset Input
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection