@extends('layouts.master')

@section('content')
<div class="col-12">
  <div class="card card-outline card-primary">
    <div class="card-header">
      <h5 class="card-title">
        Data Kategori
      </h5>
      <div class="card-tools">
        <a href="{{ route('category.create') }}" class="btn btn-xs btn-primary"> &ensp; <i class="fa fa-plus"></i> &ensp; Tambah Data Kategori</a>
      </div>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="category-table">
          <thead>
            <tr>
              <th>Nama Kategori</th>
              <th>Jumlah Produk</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
						@forelse ($category as $item)
						
            <tr>
              <td>{{ $item->name }}</td>
              <td>{{ count($item->product()->get()) }} Produk</td>
              <td width="10%" class="text-center">
                <div class="btn-group">
                  <a href="{{ route('category.edit', $item->id) }}" class="btn btn-sm btn-warning">
                    <i class="fa fa-edit"></i>
									</a>
									<form action="{{ route('category.destroy', $item->id) }}" method="post">
										@csrf
										@method('DELETE')
										<button type="submit" href="{{ route('category.destroy', $item->id) }}" class="btn btn-sm btn-danger">
											<i class="fa fa-trash"></i>
										</button>
									</form>
                </div>
              </td>
            </tr>
						@empty
						<tr>
							<td colspan="3" class="text-center">
								<h5>Belum Ada Data</h5>
							</td>
						</tr>
						@endforelse
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script>
  $(document).ready(function() {
    $('#category-table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endsection