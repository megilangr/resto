<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosDetail extends Model
{
	protected $fillable = [
		'pos_header_id', 'product_id', 'qty', 'price' 
	];

	public function header()
	{
		return $this->belongsTo('App\PosHeader', 'id', 'pos_header_id');
	}

	public function product()
	{
		return $this->hasOne('App\Product', 'id', 'product_id');
	}
}

