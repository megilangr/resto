<?php

namespace App\Http\Controllers;

use App\PosDetail;
use App\PosHeader;
use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Facades\DB;
use PDF;

class PosController extends Controller
{
    public function index()
    {
			return view('pos.index');
		}
		
		public function store()
		{
			$cart = Cart::session(auth()->user()->id);
			if ($cart->isEmpty()) {
				echo "<script>window.close();</script>";
			} else {
				try {
					DB::beginTransaction();

					$pos = PosHeader::create([
						'date' => date('Y-m-d'),
						'total' => $cart->getTotal(),
						'user_id' => auth()->user()->id
					]);

					foreach ($cart->getContent() as $key => $value) {
						$detail = PosDetail::create([
							'pos_header_id' => $pos->id,
							'product_id' => $value->id,
							'qty' => $value->quantity,
							'price' => $value->price,
						]);
					}

					DB::commit();

					$cart->clear();

					$pdf = PDF::loadview('pos.print', compact('pos'));
					return $pdf->stream('pos-'.time().'-print');

				} catch (\Exception $e) {
					DB::rollback();
					session()->flash('error', 'Terjadi Kesalahan !');
					echo "<script>window.close();</script>";
				}
			}
		}

		public function reset()
		{
			$cart = Cart::session(auth()->user()->id)->clear();
			return redirect(route('pos.index'));
		}

		public function print($id)
		{
			try {
				$pos = PosHeader::findOrFail($id);
				$pdf = PDF::loadview('pos.print', compact('pos'));
				return $pdf->stream('pos-'.time().'-print');
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function list()
		{
			try {
				$pos = PosHeader::with('detail')->orderBy('created_at', 'DESC')->get();
				return view('pos.list', compact('pos'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function printAll(Request $request)
		{
			try {
				$pos = PosHeader::with('detail')->orderBy('created_at', 'DESC')->get();
				if ($request->get('date')) {
					$pos = PosHeader::with('detail')->where('created_at', 'like', $request->date.'%')->get();
				}

				if ($request->get('month')) {
					$pos = PosHeader::with('detail')->where('created_at', 'like', $request->month.'%')->get();
				}
				$pdf = PDF::loadview('pos.printall', compact('pos'));
				return $pdf->stream('pos-'.time().'-printall');
			} catch (\Exception $e) {
				dd($e);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back()->withInput();
			}
		}
}
