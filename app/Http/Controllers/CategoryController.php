<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$category = Category::orderBy('name', 'ASC')->get();
			return view('category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$this->validate($request, [
				'name' => 'required|string'
			]);

			try {
				$category = Category::firstOrCreate([
					'name' => $request->name,
					'slug' => strtolower(str_replace(' ', '-', $request->name))
				]);

				session()->flash('success', 'Data Kategori di-Tambahakan !');
				return redirect(route('category.index'));
			} catch (\Exception $e) {
				return redirect()->back('error', 'Terjadi Kesalahan !');
			}
		}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
			try {
				$edit = Category::findOrFail($id);
				return view('category.edit', compact('edit'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
			$this->validate($request, [
				'name' => 'required|string'
			]);

			try {
				$category = Category::findOrFail($id);
				$category->update([
					'name' => $request->name,
					'slug' => strtolower(str_replace(' ', '-', $request->name))
				]);

				session()->flash('info', 'Data Kategori di-Ubah !');
				return redirect(route('category.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			try {
				$category = Category::findOrFail($id);
				$category->delete();

				session()->flash('warning', 'Data Kategori di-Hapus !');
				return redirect(route('category.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}
