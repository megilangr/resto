<?php

namespace App\Http\Controllers;

use App\PosHeader;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
				$user = auth()->user();
				$date = date('Y-m-d');
				$product = Product::count();
				$pos = PosHeader::where('created_at', 'LIKE', $date.'%');
				$count = $pos->count();
				$total = $pos->sum('total');
				return view('main', compact('product', 'count', 'total'));
		}
		
		public function changePassword(Request $request)
		{
			$this->validate($request, [
				'old_password' => 'required|string',
				'password' => 'required|string|confirmed'
			]);

			try {
				$user = Auth::user();
				if (Auth::attempt(['email' => $user->email, 'password' => $request->old_password])) {
					$user = User::findOrFail($user->id);
					$user->update([
						'password' => bcrypt($request->password)
					]);
				} else {
					session()->flash('warning', 'Password Salah !');
					return redirect()->back();
				}

				session()->flash('info', 'Password di-Ubah !');
				return redirect(route('home'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}
