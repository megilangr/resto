<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			$product = Product::orderBy('name', 'ASC')->get();
			return view('product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
			$category = Category::orderBy('name', 'ASC')->get();
			return view('product.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
			$this->validate($request, [
				'name' => 'required|string',
				'category_id' => 'required|string|exists:categories,id',
				'price' => 'required|numeric',
				'description' => 'required|string',
				'image' => 'nullable|file'
			]);
			try {
				$file = '';
				if ($request->hasFile('image')) {
					$name = $request->name . '-' . rand(1000, 9999) . '.' . $request->image->getClientOriginalExtension();
					$path = $request->file('image')->storeAs('product', $name, 'images');
					$file = $name;
				}

				$product = Product::firstOrCreate([
					'name' => $request->name,
					'category_id' => $request->category_id,
					'price' => $request->price,
					'description' => $request->description,
					'image' => $file
				]);

				session()->flash('success', 'Data Produk di-Tambahkan !');
				return redirect(route('product.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back()->withInput();
			}
		}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
			try {
				$edit = Product::findOrFail($id);
				$category = Category::orderBy('name', 'ASC')->get();

				return view('product.edit', compact('edit', 'category'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
			$this->validate($request, [
				'name' => 'required|string',
				'category_id' => 'required|string|exists:categories,id',
				'price' => 'required|numeric',
				'description' => 'required|string',
				'image' => 'nullable|file'
			]);

			try {
				$product = Product::findOrFail($id);

				$file = '';
				if ($request->hasFile('image')) {
					Storage::disk('images')->delete('product/' . $product->image);
					$name = $request->name . '-' . rand(1000, 9999) . '.' . $request->image->getClientOriginalExtension();
					$path = $request->file('image')->storeAs('product', $name, 'images');
					$file = $name;
				} else {
					$file = $product->image;
				}

				$product->update([
					'name' => $request->name,
					'category_id' => $request->category_id,
					'price' => $request->price,
					'description' => $request->description,
					'image' => $file
				]);

				session()->flash('info', 'Data Produk di-Ubah !');
				return redirect(route('product.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			try {
				$product = Product::findOrFail($id);
				Storage::disk('images')->delete('product/' . $product->image);
				$product->delete();

				session()->flash('warning', 'Data Produk di-Hapus !');
				return redirect(route('product.index'));
			} catch (\Exception $e) {
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}

		public function print()
		{
			try {
				$product = Product::orderBy('name', 'ASC')->get();
				$pdf = PDF::loadview('product.print', compact('product'));
				return $pdf->stream('product-pdf');
			} catch (\Exception $e) {
				dd($e);
				session()->flash('error', 'Terjadi Kesalahan !');
				return redirect()->back();
			}
		}
}

