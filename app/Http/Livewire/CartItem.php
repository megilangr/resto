<?php

namespace App\Http\Livewire;

use App\Product;
use Livewire\Component;
use Cart;

class CartItem extends Component
{
		public $cart = [];
		public $total = 0;
		public $bayar;
		public $kembalian = 0;

		protected $listeners = ['selected' => 'selecting'];

		public function mount()
		{
			$this->getCart();
		}

    public function render()
    {
			return view('livewire.cart-item');
		}
		
		public function selecting($prod)
		{
			$cart = Cart::session(auth()->user()->id)->add($prod['id'], $prod['name'], $prod['price'], 1, []);
			$this->getCart();
		}

		public function updatedBayar($value)
		{
			$this->kembalian = (double) $this->bayar - (double) $this->total;
		}


		public function updatedTotal($value)
		{
			$this->kembalian = (double) $this->bayar - (double) $this->total;
		}

		public function getCart()
		{
			$this->cart = [];
			$cart = Cart::session(auth()->user()->id);
			foreach ($cart->getContent() as $key => $value) {
				array_push($this->cart, [
					'id' => $value->id,
					'name' => $value->name,
					'price' => $value->price,
					'quantity' => $value->quantity,
					'attributes' => $value->attributes,
					'subtotal' => $value->getPriceSum(),
				]);
			}
			$this->total = $cart->getTotal();
			$this->kembalian = 0 - (double) $this->total;
		}

		public function kurang($id)
		{
			$cart = Cart::session(auth()->user()->id);
			$cek = $cart->get($id)->quantity;
			if ($cek <= 1) {
				$cart->remove($id);
			} else {
				$update = $cart->update($id, [
					'quantity' => -1
				]);
			}
			$this->getCart();
		}

		public function tambah($id)
		{
			$cart = Cart::session(auth()->user()->id);
			$add = $cart->update($id, [
				'quantity' => +1
			]);
			$this->getCart();
		}
		
		public function deleteitem($id)
		{
			$cart = Cart::session(auth()->user()->id);
			$cart->remove($id);
			$this->getCart();
		}
}
