<?php

namespace App\Http\Livewire;

use App\Product;
use Livewire\Component;

class PosProductList extends Component
{
		public $cari;
		public $selected;
		public $product = [];

		public function mount()
		{
			$this->product = Product::orderBy('name', 'ASC')->get();
		}

    public function render()
    {
			return view('livewire.pos-product-list');
		}

		public function updatedCari($value)
		{
			if ($this->cari == '') {
				$this->product = [];
				$product = Product::orderBy('name', 'ASC')->get();
				$this->product = $product; 
			} else {
				$this->product = [];
				$product = Product::where('name', 'LIKE', '%'.$this->cari.'%')->get();
				$this->product = $product;
			}
		}

		public function select($id)
		{
			$this->selected = $id;
			$product = Product::findOrFail($id);
			$this->emit('selected', $product);
			$this->selected = $product;
		}

}
