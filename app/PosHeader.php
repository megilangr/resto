<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PosHeader extends Model
{
	protected $fillable = [
		'date', 'total', 'user_id'
	];

	public function detail()
	{
		return $this->hasMany('App\PosDetail', 'pos_header_id', 'id');
	}

	public function user()
	{
		return $this->hasOne('App\User', 'id', 'user_id');
	}
}
