<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'name',
		'category_id',
		'price',
		'description',
		'image',
	];

	public function category()
	{
		return $this->hasOne('App\Category', 'id', 'category_id');
	}

	public function pos()
	{
		return $this->belongsTo('App\PosDetail', 'product_id', 'id');
	}
}

