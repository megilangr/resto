<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
	Route::group(['middleware' => ['role:admin']], function () {
		Route::resource('category', 'CategoryController');
		Route::resource('product', 'ProductController');
		Route::resource('user', 'UserController');
	});

	Route::group(['middleware' => ['role:kasir|admin']], function () {
		Route::get('/pos', 'PosController@index')->name('pos.index');
		Route::post('/pos', 'PosController@store')->name('pos.store');
		Route::get('/pos/reset', 'PosController@reset')->name('pos.reset');	
	});

	Route::group(['middleware' => ['role:owner|admin']], function () {
		Route::get('product/print/all', 'ProductController@print')->name('product.print');
		Route::get('/pos/print-all', 'PosController@printAll')->name('pos.printAll');
	});

	Route::group(['middleware' => ['role:owner|admin|kasir']], function () {
		Route::get('/pos/list', 'PosController@list')->name('pos.list');
		Route::get('/pos/print/{id}', 'PosController@print')->name('pos.print');
	});

	Route::post('/change-password', 'HomeController@changePassword')->name('change-password');
});